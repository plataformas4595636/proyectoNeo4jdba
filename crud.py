from neo4j import GraphDatabase

class Neo4jCRUD:
    def __init__(self, uri="bolt://localhost:7687", user="neo4j", password="contraseña123"):
        self._uri = uri
        self._user = user
        self._password = password
        self._driver = GraphDatabase.driver(self._uri, auth=(self._user, self._password))

    def close(self):
        self._driver.close()

    def create_emp(self, emp_data):
        with self._driver.session() as session:
            session.write_transaction(self._create_emp, emp_data)

    @staticmethod
    def _create_emp(tx, emp_data):
        tx.run("CREATE (e:Emp {EMPNO: $EMPNO, ENAME: $ENAME, JOB: $JOB, MGR: $MGR, HIREDATE: $HIREDATE, SAL: $SAL, COMM: $COMM, DEPTNO: $DEPTNO})",
               EMPNO=emp_data["EMPNO"], ENAME=emp_data["ENAME"], JOB=emp_data["JOB"], MGR=emp_data["MGR"],
               HIREDATE=emp_data["HIREDATE"].isoformat(), SAL=emp_data["SAL"], COMM=emp_data["COMM"], DEPTNO=emp_data["DEPTNO"])

    def read_emp(self, empno):
        with self._driver.session() as session:
            return session.read_transaction(self._read_emp, empno)

    @staticmethod
    def _read_emp(tx, empno):
        result = tx.run("MATCH (e:Emp {EMPNO: $EMPNO}) RETURN e", EMPNO=empno)
        record = result.single()
        if record:
            return record["e"]
        else:
            return None

    def update_emp(self, emp_data):
        with self._driver.session() as session:
            session.write_transaction(self._update_emp, emp_data)

    @staticmethod
    def _update_emp(tx, emp_data):
        tx.run("MATCH (e:Emp {EMPNO: $EMPNO}) SET e.ENAME = $ENAME, e.JOB = $JOB, e.MGR = $MGR, e.HIREDATE = $HIREDATE, "
               "e.SAL = $SAL, e.COMM = $COMM, e.DEPTNO = $DEPTNO",
               EMPNO=emp_data["EMPNO"], ENAME=emp_data["ENAME"], JOB=emp_data["JOB"], MGR=emp_data["MGR"],
               HIREDATE=emp_data["HIREDATE"].isoformat(), SAL=emp_data["SAL"], COMM=emp_data["COMM"], DEPTNO=emp_data["DEPTNO"])

    def delete_emp(self, empno):
        with self._driver.session() as session:
            session.write_transaction(self._delete_emp, empno)

    @staticmethod
    def _delete_emp(tx, empno):
        tx.run("MATCH (e:Emp {EMPNO: $EMPNO}) DETACH DELETE e", EMPNO=empno)

    # Métodos CRUD para departamentos

    # Métodos CRUD para departamentos
    def create_dept(self, dept_data):
        with self._driver.session() as session:
            session.write_transaction(self._create_dept, dept_data)

    @staticmethod
    def _create_dept(tx, dept_data):
        tx.run("CREATE (d:Dept {DEPTNO: $DEPTNO, DNAME: $DNAME, LOC: $LOC})",
               DEPTNO=dept_data["DEPTNO"], DNAME=dept_data["DNAME"], LOC=dept_data["LOC"])

    def read_dept(self, deptno):
        with self._driver.session() as session:
            return session.read_transaction(self._read_dept, deptno)

    @staticmethod
    def _read_dept(tx, deptno):
        result = tx.run("MATCH (d:Dept {DEPTNO: $DEPTNO}) RETURN d", DEPTNO=deptno)
        record = result.single()
        if record:
            return record["d"]
        else:
            return None

    def update_dept(self, dept_data):
        with self._driver.session() as session:
            session.write_transaction(self._update_dept, dept_data)

    @staticmethod
    def _update_dept(tx, dept_data):
        tx.run("MATCH (d:Dept {DEPTNO: $DEPTNO}) SET d.DNAME = $DNAME, d.LOC = $LOC",
               DEPTNO=dept_data["DEPTNO"], DNAME=dept_data["DNAME"], LOC=dept_data["LOC"])

    def delete_dept(self, deptno):
        with self._driver.session() as session:
            session.write_transaction(self._delete_dept, deptno)

    @staticmethod
    def _delete_dept(tx, deptno):
        tx.run("MATCH (d:Dept {DEPTNO: $DEPTNO}) DETACH DELETE d", DEPTNO=deptno)
