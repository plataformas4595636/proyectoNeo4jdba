# Proyecto de Gestión de Empleados y Departamentos con Neo4j y Python

Este proyecto es una aplicación de gestión de empleados y departamentos que utiliza una base de datos Neo4j y está implementado en Python. Permite realizar operaciones CRUD (Crear, Leer, Actualizar, Eliminar) tanto para empleados como para departamentos.

## Requisitos

- Python 3.x instalado en tu sistema.
- Una base de datos Neo4j en ejecución.
- El driver de Neo4j para Python instalado. Puedes instalarlo con pip:

    ```bash
    pip install neo4j
    ```

## Estructura del Proyecto

El proyecto consta de los siguientes archivos:

- `main.py`: Este archivo contiene el código principal de la aplicación, que maneja la interacción con el usuario a través de un menú y llama a las funciones correspondientes en el módulo `crud.py`.
- `crud.py`: Aquí se encuentran las funciones para realizar operaciones CRUD en la base de datos Neo4j, como crear, leer, actualizar y eliminar empleados y departamentos.
- `database.py`: Este archivo contiene la clase `Database` que se encarga de establecer la conexión con la base de datos Neo4j y proporcionar métodos para ejecutar consultas Cypher.
- Otros archivos: Puedes incluir cualquier otro archivo necesario para tu implementación, como scripts Cypher para crear la estructura de la base de datos.

## Uso

Para ejecutar la aplicación, simplemente ejecuta el archivo `main.py` con Python:

```bash
python main.py
```


## Authors and acknowledgment

Brayan Ricardo Carrete Martínez - 307746

## License
Sin licencia

## Archivos Cypher
Actualmente no me dejo descargar los archivos me salia error de conexión, de igual manera le adjunto, la creación de departamentos, de empleados, así como la relaciones entre estos

```
////Crear los departamentos
        CREATE (:Dept {DEPTNO: 10, DNAME: 'ACCOUNTING', LOC: 'NEW YORK'}),
       (:Dept {DEPTNO: 20, DNAME: 'RESEARCH', LOC: 'DALLAS'}),
       (:Dept {DEPTNO: 30, DNAME: 'SALES', LOC: 'CHICAGO'}),
       (:Dept {DEPTNO: 40, DNAME: 'OPERATIONS', LOC: 'BOSTON'});
```
```
////Crear los empleados
CREATE (:Emp {EMPNO: 7369, ENAME: 'SMITH', JOB: 'CLERK', MGR: 7902, HIREDATE: date('1980-12-17'), SAL: 800, COMM: NULL, DEPTNO: 20}),
       (:Emp {EMPNO: 7499, ENAME: 'ALLEN', JOB: 'SALESMAN', MGR: 7698, HIREDATE: date('1981-02-20'), SAL: 1600, COMM: 300, DEPTNO: 30}),
       (:Emp {EMPNO: 7521, ENAME: 'WARD', JOB: 'SALESMAN', MGR: 7698, HIREDATE: date('1981-02-22'), SAL: 1250, COMM: 500, DEPTNO: 30}),
       (:Emp {EMPNO: 7566, ENAME: 'JONES', JOB: 'MANAGER', MGR: 7839, HIREDATE: date('1981-04-02'), SAL: 2975, COMM: NULL, DEPTNO: 20}),
       (:Emp {EMPNO: 7654, ENAME: 'MARTIN', JOB: 'SALESMAN', MGR: 7698, HIREDATE: date('1981-09-28'), SAL: 1250, COMM: 1400, DEPTNO: 30}),
       (:Emp {EMPNO: 7698, ENAME: 'BLAKE', JOB: 'MANAGER', MGR: 7839, HIREDATE: date('1981-05-01'), SAL: 2850, COMM: NULL, DEPTNO: 30}),
       (:Emp {EMPNO: 7782, ENAME: 'CLARK', JOB: 'MANAGER', MGR: 7839, HIREDATE: date('1981-06-09'), SAL: 2450, COMM: NULL, DEPTNO: 10}),
       (:Emp {EMPNO: 7788, ENAME: 'SCOTT', JOB: 'ANALYST', MGR: 7566, HIREDATE: date('1987-05-10'), SAL: 3000, COMM: NULL, DEPTNO: 20}),
       (:Emp {EMPNO: 7839, ENAME: 'KING', JOB: 'PRESIDENT', MGR: NULL, HIREDATE: date('1981-11-17'), SAL: 5000, COMM: NULL, DEPTNO: 10}),
       (:Emp {EMPNO: 7844, ENAME: 'TURNER', JOB: 'SALESMAN', MGR: 7698, HIREDATE: date('1981-09-08'), SAL: 1500, COMM: 0, DEPTNO: 30}),
       (:Emp {EMPNO: 7876, ENAME: 'ADAMS', JOB: 'CLERK', MGR: 7788, HIREDATE: date('1987-05-29'), SAL: 1100, COMM: NULL, DEPTNO: 20}),
       (:Emp {EMPNO: 7900, ENAME: 'JAMES', JOB: 'CLERK', MGR: 7698, HIREDATE: date('1981-12-03'), SAL: 950, COMM: NULL, DEPTNO: 30}),
       (:Emp {EMPNO: 7902, ENAME: 'FORD', JOB: 'ANALYST', MGR: 7566, HIREDATE: date('1981-12-03'), SAL: 3000, COMM: NULL, DEPTNO: 20}),
       (:Emp {EMPNO: 7934, ENAME: 'MILLER', JOB: 'CLERK', MGR: 7782, HIREDATE: date('1982-01-23'), SAL: 1300, COMM: NULL, DEPTNO: 10});
```
```
       ////Crear las relaciones
       MATCH (e:Emp), (d:Dept)
        WHERE e.DEPTNO = d.DEPTNO
        CREATE (e)-[:WORKS_FOR]->(d);
```