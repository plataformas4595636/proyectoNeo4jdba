from crud import Neo4jCRUD
from datetime import datetime

# Instanciar la clase Neo4jCRUD
neo4j_crud = Neo4jCRUD()

# Funciones CRUD para empleados
def create_emp():
    emp_data = {
        "EMPNO": int(input("Introduce el número de empleado (EMPNO): ")),
        "ENAME": input("Introduce el nombre del empleado (ENAME): "),
        "JOB": input("Introduce el puesto del empleado (JOB): "),
        "MGR": int(input("Introduce el número del gerente del empleado (MGR): ")),
        "HIREDATE": datetime.strptime(input("Introduce la fecha de contratación del empleado (en formato YYYY-MM-DD): "), "%Y-%m-%d"),
        "SAL": float(input("Introduce el salario del empleado (SAL): ")),
        "COMM": float(input("Introduce la comisión del empleado (COMM): ")),
        "DEPTNO": int(input("Introduce el número de departamento del empleado (DEPTNO): "))
    }
    neo4j_crud.create_emp(emp_data)
    print("Empleado creado exitosamente.")

def read_emp():
    empno = int(input("Introduce el número de empleado (EMPNO) que deseas buscar: "))
    emp = neo4j_crud.read_emp(empno)
    if emp:
        print(emp)
    else:
        print("No se encontró ningún empleado con ese número.")

def update_emp():
    empno = int(input("Introduce el número de empleado (EMPNO) que deseas actualizar: "))
    emp = neo4j_crud.read_emp(empno)
    if emp:
        emp_data = {
            "EMPNO": empno,
            "ENAME": input(f"Nuevo nombre del empleado (ENAME) [{emp['ENAME']}]: ") or emp['ENAME'],
            "JOB": input(f"Nuevo puesto del empleado (JOB) [{emp['JOB']}]: ") or emp['JOB'],
            "MGR": int(input(f"Nuevo número del gerente del empleado (MGR) [{emp['MGR']}]: ")) or emp['MGR'],
            "HIREDATE": input(f"Nueva fecha de contratación del empleado (en formato YYYY-MM-DD) "
                              f"[{emp['HIREDATE'].iso_format()}]: ") or emp['HIREDATE'].iso_format(),
            "SAL": float(input(f"Nuevo salario del empleado (SAL) [{emp['SAL']}]: ")) or emp['SAL'],
            "COMM": float(input(f"Nueva comisión del empleado (COMM) [{emp['COMM']}]: ")) or emp['COMM'],
            "DEPTNO": int(input(f"Nuevo número de departamento del empleado (DEPTNO) [{emp['DEPTNO']}]: ")) or emp['DEPTNO']
        }
        neo4j_crud.update_emp(emp_data)
        print("Empleado actualizado exitosamente.")
    else:
        print("No se encontró ningún empleado con ese número.")

def delete_emp():
    empno = int(input("Introduce el número de empleado (EMPNO) que deseas eliminar: "))
    emp = neo4j_crud.read_emp(empno)
    if emp:
        neo4j_crud.delete_emp(empno)
        print("Empleado eliminado exitosamente.")
    else:
        print("No se encontró ningún empleado con ese número.")

# Funciones CRUD para departamentos
def create_dept():
    dept_data = {
        "DEPTNO": int(input("Introduce el número de departamento (DEPTNO): ")),
        "DNAME": input("Introduce el nombre del departamento (DNAME): "),
        "LOC": input("Introduce la ubicación del departamento (LOC): ")
    }
    neo4j_crud.create_dept(dept_data)
    print("Departamento creado exitosamente.")

def read_dept():
    deptno = int(input("Introduce el número de departamento (DEPTNO) que deseas buscar: "))
    dept = neo4j_crud.read_dept(deptno)
    if dept:
        print(dept)
    else:
        print("No se encontró ningún departamento con ese número.")

def update_dept():
    deptno = int(input("Introduce el número de departamento (DEPTNO) que deseas actualizar: "))
    dept = neo4j_crud.read_dept(deptno)
    if dept:
        dept_data = {
            "DEPTNO": deptno,
            "DNAME": input(f"Nuevo nombre del departamento (DNAME) [{dept['DNAME']}]: ") or dept['DNAME'],
            "LOC": input(f"Nueva ubicación del departamento (LOC) [{dept['LOC']}]: ") or dept['LOC']
        }
        neo4j_crud.update_dept(dept_data)
        print("Departamento actualizado exitosamente.")
    else:
        print("No se encontró ningún departamento con ese número.")

def delete_dept():
    deptno = int(input("Introduce el número de departamento (DEPTNO) que deseas eliminar: "))
    dept = neo4j_crud.read_dept(deptno)
    if dept:
        neo4j_crud.delete_dept(deptno)
        print("Departamento eliminado exitosamente.")
    else:
        print("No se encontró ningún departamento con ese número.")

# Menú principal
def main():
    while True:
        print("\n*** Menú de Operaciones ***")
        print("1. Crear empleado")
        print("2. Leer empleado")
        print("3. Actualizar empleado")
        print("4. Eliminar empleado")
        print("5. Crear departamento")
        print("6. Leer departamento")
        print("7. Actualizar departamento")
        print("8. Eliminar departamento")
        print("9. Salir")

        option = int(input("Selecciona una opción: "))

        if option == 1:
            create_emp()
        elif option == 2:
            read_emp()
        elif option == 3:
            update_emp()
        elif option == 4:
            delete_emp()
        elif option == 5:
            create_dept()
        elif option == 6:
            read_dept()
        elif option == 7:
            update_dept()
        elif option == 8:
            delete_dept()
        elif option == 9:
            print("Saliendo del programa...")
            break
        else:
            print("Opción inválida. Inténtalo de nuevo.")

if __name__ == "__main__":
    main()
